Changelog
=========


1.0.2 (unreleased)
------------------

- Add parameter to choose which held position states are used to add
  persons to organization group. All positions are used if parameter is not set.
  [cedricmessiant]

- Fix getGroupsForPrincipal so it does not crash if no position
  [ebrehault]

- Do not crash Person membrane behavior if Group membrane behavior is not assigned to
  organizations [ebrehault]


1.0.1 (2014-09-15)
------------------

- Change i18n domain in person types xml (collective.contact.core)


1.0 (2014-06-18)
----------------

- Initial release.
  [cedricmessiant]

